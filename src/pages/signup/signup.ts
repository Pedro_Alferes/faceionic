import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the SignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
user={name:"",password:"",confirmPassword:"",email:"",confirmEmail:""}

  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  formSubmit(){
      let loader = this.loadingCtrl.create({
        content: "Cadastrando....",
        duration: 3000
      });
      loader.present();
      if(this.user.name == this.user.name && this.user.password == this.user.password && this.user.confirmPassword ==this.user.confirmPassword && this.user.email == this.user.email
      && this.user.confirmEmail == this.user.confirmEmail){
        this.navCtrl.pop();
        loader.dismiss();
      }
    }
  }
  



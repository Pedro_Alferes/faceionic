import { TabsPage } from './../tabs/tabs';
import { ForgotPasswordPage } from './../forgot-password/forgot-password';
import { SignupPage } from './../signup/signup';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the SigninPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
public email:String="";
public password:String="";


  constructor(public navCtrl: NavController, public toastCtrl:ToastController,public navParams: NavParams,public loadingCtrl: LoadingController) {
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
   
  }
  
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Digite sua senha e login para se autenticar',
      duration: 3000
    });
    toast.present();
  }
  signin(){
    
      let loader = this.loadingCtrl.create({
        content: "Autenticando...",
        duration: 3000,
      });
      loader.present();
      if(this.email=="" || this.password==""){
        loader.dismiss();
        this.presentToast();    
      }
      else if( this.email == this.email && this.password==this.password){
        this.navCtrl.push(TabsPage);
        loader.dismiss(); 
      } 
    }
    signup(){
      this.navCtrl.push(SignupPage);
  }
    forgotPassword(){
      this.navCtrl.push(ForgotPasswordPage);
  }
}

    


  




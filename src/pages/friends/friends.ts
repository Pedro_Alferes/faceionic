import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController} from 'ionic-angular';
/**
 * Generated class for the FriendsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {
  friends = ['Alida','Tiago','Rodrigo'];
  newFriend ="";
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController) {
  }


  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Friend',
      message: 'Do you want add this person?'+ this.newFriend,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
           console.log("click!!!");
          }
        },
        {
          text: 'Yes',
          handler: () => {
            if(this.newFriend !=""){
              this.friends.push(this.newFriend);
              this.newFriend="";
            }        
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendsPage');
  }
  
  removeFriend= function(index){
    this.friends.splice(index,1);
  }

}

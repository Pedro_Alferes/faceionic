import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  email:String="";
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,public toastCtrl: ToastController) {
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Email enviado com sucesso',
      duration: 3000
    });
    toast.present();
  }

  sendPassword(){
    let loader = this.loadingCtrl.create({
      content: "Enviando....",
      duration: 3000
    });
    loader.present();
     if(this.email == ""){
      let warning = this.toastCtrl.create({
        message: 'Por favor informe o Email',
        duration: 3000
      });
      warning.present();
    }
    else if(this.email ==this.email){
      loader.dismiss();
      this.presentToast();
      this.navCtrl.pop();   
    }
  } 
}
